#vim: set fileencoding=utf-8:
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.test.client import RequestFactory
from django.test import Client
from views import *

__author__ = 'ADRIANA MARCELA OSORIO'

class TestVerRecurso(TestCase):
    """
        Validar que exista la url de ingresar informacion avanzada de recurso educativo
    """

    def test_urlVerRecurso(self):
        url = reverse('verRecurso',args=[31])
        self.assertEqual(url, '/recurso/31/')

    '''  Validar que en el html exista el boton de eliminar'''


    def test_htmlEliminar(self):
        os.environ['EMAIL_DSPACE'] = 'jc.cerquera10@uniandes.edu.co'
        os.environ['PASSWORD_DSPACE'] = 'dspace'
        os.environ['DNS_DSPACE'] = 'ec2-54-86-20-91.compute-1.amazonaws.com'
        client= Client()
        response = client.get('/recurso/31/')
        self.assertContains(response,'informacion',3)



    '''  Validar que en el html exista el boton descargar'''


    def test_htmlDescargar(self):
        client= Client()
        os.environ['EMAIL_DSPACE'] = 'jc.cerquera10@uniandes.edu.co'
        os.environ['PASSWORD_DSPACE'] = 'dspace'
        os.environ['DNS_DSPACE'] = 'ec2-54-86-20-91.compute-1.amazonaws.com'
        response = client.get('/recurso/31/')
        self.assertContains(response,'descargar',1)


    '''   Validar que en el html se incluya la imagen verRecurso.png'''
    def test_htmlImagen(self):
        os.environ['EMAIL_DSPACE'] = 'jc.cerquera10@uniandes.edu.co'
        os.environ['PASSWORD_DSPACE'] = 'dspace'
        os.environ['DNS_DSPACE'] = 'ec2-54-86-20-91.compute-1.amazonaws.com'
        client= Client()
        response = client.get('/recurso/31/')
        self.assertContains(response,'verRecurso.png',1)


    '''Validar servicio rest que consulta la información de un recurso educativo'''

    def test_serviceVerRecurso(self):
        client = Client()
        os.environ['EMAIL_DSPACE'] = 'jc.cerquera10@uniandes.edu.co'
        os.environ['PASSWORD_DSPACE'] = 'dspace'
        os.environ['DNS_DSPACE'] = 'ec2-54-86-20-91.compute-1.amazonaws.com'
        response = client.get('/recurso/31/')

        self.assertEqual(response.status_code, 200)
