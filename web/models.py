#vim: set fileencoding=utf-8:
from django.db import models
from django import forms
from django.forms import ModelForm
from django.forms.widgets import DateInput, TextInput, Textarea, Select,DateTimeInput
import datetime
import requests
import os



def obtieneAreas():
    IndId = 0
    IndName = 0
    AreaId = 0
    AreaName = "-Seleccione Área-"
    communities=[]
    communities.append((AreaId, AreaName),)
    AreaName = ""
    r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/communities/")
    #r = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/communities/")
    r1 = r.text.replace(":",",")
    r2 = r1.replace('"',"")
    r3 = r2.replace("{","")
    r4 = r3.replace("[","")
    datos = r4.split(',')
    for dato in datos:
        if (IndId==1):
            AreaId = dato
            IndId = 2
        else:
            if (IndName==1):
                AreaName = dato
                IndName = 2

        if dato=="id":
            IndId = 1
        else:
            if dato=="name":
                IndName = 1

        if (IndId==2 and IndName==2):
            communities.append((AreaId, AreaName),)
            IndId = 0
            IndName = 0
            AreaId = 0
            AreaName = ""
    return communities

#def obtieneCategorias(IdArea):
def obtieneCategorias():
    IndId = 0
    IndName = 0
    CategoriaId = 0
    CategoriaName = ""
    r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/collections")
    #r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/communities/"+ IdArea + "/collections")
    #r = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/collections")
    #r = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/communities/"+ IdArea + "/collections")
    r1 = r.text.replace(":",",")
    r2 = r1.replace('"',"")
    r3 = r2.replace("{","")
    r4 = r3.replace("[","")
    datos = r4.split(',')
    collections=[]
    for dato in datos:
        if (IndId==1):
            CategoriaId = dato
            IndId = 2
        else:
            if (IndName==1):
                CategoriaName = dato
                IndName = 2

        if dato=="id":
            IndId = 1
        else:
            if dato=="name":
                IndName = 1

        if (IndId==2 and IndName==2):
            collections.append((CategoriaId, CategoriaName),)
            IndId = 0
            IndName = 0
            CategoriaId = 0
            CategoriaName = ""
    return collections

# Listas de Area Conocimiento con sus Categorías
AreaChoices=  (
        ('Ciencias de la Educacion', []),
        ('Ciencias de la Salud', []),
        ('Ciencias Sociales y Humanas', []),
        ('Ingenieria, Arquitectura, Urbanismo y afines', [(1, "Arquitectura"),(2, "Arquitectura y afines"),(4, "Ingenieria Civil"),(3, "Ingenieria de sistemas, telematica y afines")]),
        ('Ingenieria de Sistemas', []),
        ('Ingenieria de Sistemas', []),
        ('Ingenieria de Sistemas', []),
        ('Matematicas y Ciencias Naturales', []),
        )

# Listas de seleccion de las pantallas
# Opciones de Nivel de recurso educativo
NivelChoices=  (
        ('Muy Alto', 'Muy Alto'),
        ('Alto', 'Alto'),
        ('Medio', 'Medio'),
        ('Bajo', 'Bajo'),
        )
# Opciones de extensiones de recurso educativo
ExtensionChoices=(
        ('Pdf', 'Pdf'),
        ('doc', 'doc'),
        ('xlsx', 'xlsx'),
        ('mp4', 'mp4'),
        ('ppt', 'ppt'),
        ('jpeg', 'jpeg'),
        ('gif', 'gif'),
        ('png', 'png'),
        )
# Opciones de paises de recurso educativo
PaisChoices=(
        ('Colombia', 'Colombia'),
        ('Argentina', 'Argentina'),
        ('Brasil', 'Brasil'),
        ('Bolivia', 'Bolivia'),
        ('Ecuador', 'Ecuador'),
        ('Perú', 'Perú'),
        )
# Opciones de idiomas de recurso educativo
LenguajeChoices=(
        ('es', 'Español'),
        ('en', 'Ingles'),
        ('fr', 'Frances'),
        ('it', 'Italiano'),
        )

# Opciones de Tipo de recurso educativo
TipRecChoices=(
        ('Audio-visual', 'Audio-visual'),
        ('Audio', 'Audio'),
        ('Libro', 'Libro'),
        )

# Opciones de Nivel Educativo de recurso educativo
NivEduChoices=(
        ('Pregrado', 'Pregrado'),
        ('Maestria', 'Maestria'),
        )


#Opciones de Interactividad
InteractividadChoices=(
    ('Expositiva','Expositiva'),
    ('Narrativa','Narrativa'),
    ('Lectura','Lectura'),
    ('Interactiva','Interactiva')
)

# Opciones de Densidad Semantica
DensidadSemanticaChoices=  (
        ('Alta', 'Alta'),
        ('Media', 'Media'),
        ('Baja', 'Baja'),
        )

# Opciones de Dificultad
DificultadChoices=  (
        ('Alta', 'Alta'),
        ('Media', 'Media'),
        ('Baja', 'Baja'),
        )

#Metadata
class DItem(models.Model):
    fecha = models.CharField(max_length=10)
    titulo = models.CharField(max_length=20)
    autor = models.CharField(max_length=20)
    descripcion = models.CharField(max_length=50)
    pais = models.CharField(max_length=20)
    keywords = models.CharField(max_length=20)
    idItem = models.IntegerField(default=0)
    reda = models.CharField(max_length=1, default='N')

class MetadataItem(models.Model):
    titulo							= models.CharField(max_length=2048)
    descripcion						= models.CharField(max_length=2048)
    autor							= models.CharField(max_length=2048)
    nivelAgregacion					= models.CharField(max_length=10)
    extension						= models.CharField(max_length=10)
    lenguaje						= models.CharField(max_length=100)
    pais   							= models.CharField(max_length=100)
    palabrasClaves					= models.CharField(max_length=2048)
    fecha	                        = models.CharField(max_length=100)
    areaConocimiento                = models.CharField(max_length=300)
    categoria                       = models.CharField(max_length=300)
    tipoRecurso                     = models.CharField(max_length=100)
    nivelEducativo                  = models.CharField(max_length=100)
    recursoWebsite					= models.CharField(max_length=2048)
    recursoEstructura				= models.CharField(max_length=2048)
    recursoVersion					= models.CharField(max_length=2048)
    recursoMomentoC					= models.CharField(max_length=2048)
    recursoCoordenadas  			= models.CharField(max_length=2048)
    recursoIdentCatalogo			= models.CharField(max_length=2048)
    recursoEsquema  				= models.CharField(max_length=2048)
    recursoCambios  				= models.CharField(max_length=2048)
    recursoLenguajeDatos			= models.CharField(max_length=2048)
    entidadNombre     				= models.CharField(max_length=2048)
    tecnicaPrevisualizacion			= models.CharField(max_length=2048)
    tecnicaInstInstalacion			= models.CharField(max_length=2048)
    tecnicaOtrosReq     			= models.CharField(max_length=2048)
    educativaInteractividad			= models.CharField(max_length=100)
    educativaDensidadSem			= models.CharField(max_length=100)
    educativaDes        			= models.CharField(max_length=2048)
    educativaRangoEdad     			= models.CharField(max_length=2048)
    educativaTiempoA     			= models.CharField(max_length=2048)
    educativaDificultad     		= models.CharField(max_length=2048)
    comentariosComent     			= models.CharField(max_length=2048)
    cometariosDesc     		        = models.CharField(max_length=2048)
    comentariosFecha				= models.CharField(max_length=2048)
    cometariosRating   		        = models.CharField(max_length=2048)
    derechosRestricciones     		= models.CharField(max_length=2048)
    derechosCosto        			= models.CharField(max_length=2048)
    derechosDesc                    = models.CharField(max_length=2048)


# Modelo de entidades correspondiente a modelo de rest-api de DSPACE
# https://wiki.duraspace.org/display/DSDOC5x/REST+API#RESTAPI-Model-Objectdatatypes

# Class Community:
# Communities in DSpace are used for organization and hierarchy, and are containers that hold
# sub-Communities and Collections. (ex: Department of Engineering)
class Community(models.Model):
    EXPAND = (
        ('parentCommunity', 'parentCommunity'),
        ('collections', 'collections'),
        ('subCommunities', 'subCommunities'),
        ('logo', 'logo'),
        ('all', 'all'),
    )
    id = models.IntegerField
    name = models.CharField(max_length=500)
    handle = models.CharField(max_length=500)
    type = models.CharField(max_length=500)
    link = models.CharField(max_length=500)
    expand = models.CharField(choices=EXPAND, max_length=500)
    logo = models.CharField(null=True, max_length=500)
    parentCommunity = models.ForeignKey('self')
    copyrightText = models.CharField(default="", max_length=500)
    introductoryText = models.CharField(default="", max_length=500)
    shortDescription = models.TextField(default="", max_length=500)
    sidebarText = models.CharField(default="", max_length=500)
    countItems = models.IntegerField
    subcommunities = models.ManyToManyField('self')


# Class Collection
# Collections in DSpace are containers of Items. (ex: Engineering Faculty Publications)
class Collection(models.Model):
    EXPAND = (
        ("parentCommunityList", "parentCommunityList"),
        ("parentCommunity", "parentCommunity"),
        ("items", "items"),
        ("license", "license"),
        ("logo", "logo"),
        ("all", "all"),
    )
    id = models.IntegerField
    name = models.CharField(max_length=500)
    handle = models.CharField(max_length=500)
    type = models.CharField(max_length=500)
    link = models.URLField
    expand = models.CharField(choices=EXPAND, max_length=500)
    logo = models.CharField(null=True, max_length=500)
    parentCommunity = models.ForeignKey(Community, related_name="community_fk", null=True)
    parentCommunityList = models.ManyToManyField(Community)
    license = models.CharField(null=True, max_length=500)
    copyrightText = models.CharField(default="", max_length=500)
    introductoryText = models.CharField(default="", max_length=500)
    shortDescription = models.TextField(default="", max_length=500)
    sidebarText = models.CharField(default="", max_length=500)
    numberItems = countItems = models.IntegerField


# Class Item
# Items in DSpace represent a "work" and combine metadata and files, known as Bitstreams.
class Item(models.Model):
    id = models.IntegerField
    name = models.CharField(max_length=500)

class ResourcePolicy(models.Model):
    id = models.IntegerField
    action = models.CharField(max_length=500)
    epersonId = models.IntegerField
    groupId = models.IntegerField
    resourceId = models.IntegerField
    resourceType = models.CharField(max_length=500)
    rpDescription = models.CharField(null=True, max_length=500)
    rpName = models.CharField(null=True, max_length=500)
    rpType = models.CharField(max_length=500)
    startDate = models.DateField
    endDate = models.DateField

# Class Bitstream
# Bitstreams are files. They have a filename, size (in bytes), and a file format. Typically in DSpace,
# the Bitstream will the "full text" article, or some other media. Some files are the actual file that
#  was uploaded (tagged with bundleName:ORIGINAL), others are DSpace-generated files that are derivatives or renditions
# , such as text-extraction, or thumbnails. You can download files/bitstreams.
# DSpace doesn't really limit the type of files that it takes in, so this could be PDF, JPG, audio, video, zip, or other.
#  Also, the logo for a Collection or a Community, is also a Bitstream.
class Bitstream(models.Model):
    EXPAND = (
        ("parent", "parent"),
        ("policies", "policies"),
        ("all", "all"),
    )
    id = models.IntegerField
    name = models.CharField(max_length=500)
    handle = models.CharField(max_length=500)
    type = models.CharField(max_length=500)
    link = models.URLField
    expand = models.CharField(choices=EXPAND, max_length=500)
    bundleName = models.CharField(max_length=500)
    description = models.TextField(default="")
    format = models.CharField(max_length=500)
    mimeType = models.CharField(max_length=500)
    sizeBytes = models.BigIntegerField
    parentObject = models.ForeignKey(Item)
    retrieveLink = models.CharField(max_length=500)
    checkSum = models.CharField(max_length=500)
    checkSumAlgorithm = models.CharField(max_length=500)
    sequenceId = models.IntegerField
    policies = models.ForeignKey(ResourcePolicy)


class MetadataEntry(models.Model):
    key = models.CharField(max_length=500)
    value = models.CharField(max_length=500)
    language = models.CharField(max_length=500)
    parentObject = models.ForeignKey(Item)

class User(models.Model):
    email = models.CharField(max_length=500)
    password = models.CharField(max_length=500)


class Status(models.Model):
    okay = models.BooleanField
    authenticated = models.BooleanField
    email = models.EmailField
    fullname = models.CharField(max_length=500)
    token = models.CharField(max_length=500)


# Creacion de Formularios customizados

# Formulario de recusro educativo
class RecursoForm1(forms.Form):
    titulo							= forms.CharField(max_length=2048, required=True,widget=TextInput(attrs={'size': 67}))
    descripcion						= forms.CharField(max_length=2048, required=True,widget=Textarea(attrs={'rows':4, 'cols': 66}))
    autor							= forms.CharField(max_length=2048, required=True,widget=TextInput(attrs={'readonly': True}),initial= "Profesor1")
    nivelAgregacion					= forms.ChoiceField(choices=NivelChoices,required=True)
    extension						= forms.ChoiceField(choices=ExtensionChoices, required=True,widget=Select)
    lenguaje						= forms.ChoiceField(choices=LenguajeChoices, required=True)
    pais   							= forms.ChoiceField(choices=PaisChoices, required=True)
    palabrasClaves					= forms.CharField(max_length=2048, required=True,widget=TextInput)
    fecha							= forms.DateTimeField(required=True,widget=DateTimeInput(attrs={'class': 'datepicker', 'readonly': True}),initial=datetime.datetime.now())


# Formulario de recusro educativo
class RecursoForm2(forms.Form):
    recurso                         = forms.FileField(required=True)

    # Formulario de recusro educativo
class RecursoForm3(forms.Form):
    #def __init__(self, IdArea, *args, **kwargs):
    #def __init__(self, *args, **kwargs):
        #super(RecursoForm3, self).__init__(*args, **kwargs)
        #req=IdArea
        #self.fields['areaConocimiento'] = forms.ChoiceField(
            #choices=obtieneAreas() )
        #self.fields['categoria'] = forms.ChoiceField(
            #choices=obtieneCategorias() )
            #choices=obtieneCategorias(IdArea=req) )
    area = forms.ChoiceField(choices=AreaChoices, required=True)
    tipoRecurso                     = forms.ChoiceField(choices=TipRecChoices,required=True)
    nivelEducativo                  = forms.ChoiceField(choices=NivEduChoices,required=True)



# Formulario de información avanzada
class RecursoForm4(forms.Form):
    recursoWebsite					= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size': 20}))
    recursoEstructura				= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size': 20}))
    recursoVersion					= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size': 20}))
    recursoMomentoC					= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size': 20}))
    recursoCoordenadas  			= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size': 20}))
    recursoIdentCatalogo			= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size': 20}))
    recursoEsquema  				= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size': 20}))
    recursoCambios  				= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size': 20}))
    recursoLenguajeDatos			= forms.ChoiceField(choices=LenguajeChoices, required=False)
    entidadNombre     				= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size':20}))
    tecnicaLocalizacion				= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size':24}))
    tecnicaPrevisualizacion			= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size':25}))
    tecnicaInstInstalacion			= forms.CharField(max_length=2048, required=False,widget=Textarea(attrs={'rows':4, 'cols': 23}))
    tecnicaOtrosReq     			= forms.CharField(max_length=2048, required=False,widget=Textarea(attrs={'rows':4, 'cols': 23}))
    educativaInteractividad			= forms.ChoiceField(choices=InteractividadChoices, required=False)
    educativaDensidadSem			= forms.ChoiceField(choices=DensidadSemanticaChoices, required=False)
    educativaDes        			= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size':25}))
    educativaRangoEdad     			= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size':25}))
    educativaTiempoA     			= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size':25}))
    educativaDificultad     		= forms.ChoiceField(choices=DificultadChoices, required=False)
    comentariosComent     			= forms.CharField(max_length=2048, required=False,widget=Textarea(attrs={'rows':4, 'cols': 25}))
    cometariosDesc     		        = forms.CharField(max_length=2048, required=False,widget=Textarea(attrs={'rows':4, 'cols': 25}))
    comentariosFecha				= forms.DateField(required=False,widget=DateInput(attrs={'class': 'datepicker'}))
    cometariosRating   		        = forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size':25}))
    derechosRestricciones     		= forms.CharField(max_length=2048, required=False,widget=Textarea(attrs={'rows':4, 'cols': 25}))
    derechosCosto        			= forms.CharField(max_length=2048, required=False,widget=TextInput(attrs={'size':25}))
    derechosDesc     		        = forms.CharField(max_length=2048, required=False,widget=Textarea(attrs={'rows':4, 'cols': 25}))

# Formulario Búsqueda Recurso Educativo por palabra clave
class RecursoForm5(forms.Form):
    def __init__(self, *args, **kwargs):
        super(RecursoForm5, self).__init__(*args, **kwargs)
        self.fields['areaConocimiento'] = forms.ChoiceField(
            choices=obtieneAreas() )

    palabraClave = forms.CharField(max_length=2048, required=False, widget=TextInput(attrs={'size': 10}))
    reda = forms.BooleanField(required=False)
