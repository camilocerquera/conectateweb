#vim: set fileencoding=utf-8:
from django.test import TestCase
from models import RecursoForm4
from django.core.urlresolvers import reverse
from django.test.client import RequestFactory
from django.test import Client
from views import *

__author__ = 'ADRIANA MARCELA OSORIO'


class TestCargarRecurso(TestCase):

  """
    Tests para validar el formulario de informacion avanzada de recurso educativo
    Se validan los campos del formulario y el ingreso no sea obligatoria
  """

  def test_formInfoAvanzada(self):
    form_data = {'recursoVersion': '1.0',
               'entidadNombre':'Nombre de la entidad' ,
               'comentariosComent':'comentariossssssss',
               'educativaDificultad':'Alta',
               'educativaDes': 'descripcion educativa',
               'cometariosDesc':'Descripcion de comentarios' }
    form = RecursoForm4(data=form_data)
    self.assertTrue(form.is_valid(),'Formulario Invalido')


  """
    Validar que exista la url de ingresar informacion avanzada de recurso educativo
  """
  def test_urlInfoAvanzada(self):
    url = reverse('info_avanzada')
    self.assertEqual(url, '/avanzada/')

  """
  Validar que en el html  de ingresar información avanzada exista la tabla de ingresar info avanzada
  """

  def test_htmlInfoAvanzada(self):
    client= Client()
    response = client.get('/avanzada/')
    self.assertContains(response,'tablaInfoAvanzada',1)

  """
  Validar funcionalidad de la vista Ingresar información avanzada de recurso educativo cuando se ingresa información y
  se presiona el botón continuar (Metodo Post). Los campos del formulairo no deben ser obligatorios
  """
  def test_viewInfoAvanzada(self):
    client = Client()
    session = client.session
    session['recurso'] = {'titulo':'titulo', 'descripcion':'descripcion','autor':'autor','nivelAgregacion':'nivelAgregacion','extension':'extension','lenguaje':'lenguaje','pais':'pais','palabrasClaves':'palabrasClaves','fecha':'fecha'}
    session.save()
    response = client.post('/avanzada/', {'recursoVersion': '1.0',
                   'entidadNombre':'Nombre de la entidad' ,
                   'comentariosComent':'comentariossssssss',
                   'educativaDificultad':'Alta',
                   'educativaDes': 'descripcion educativa',
                   'cometariosDesc':'Descripcion de comentarios' })
    self.assertEqual(response.status_code, 302,'Error en la vista')



  """Validar servicio que almacena el recurso educativo en DSPACE, incluyendo la información avanzada ingresada"""

  def test_serviceCatalogar(self):
      client = Client()
      session = client.session
      session['recurso'] = {'titulo': 'titulo',
                            'descripcion': 'descripcion',
                            'autor': 'autor',
                            'nivelAgregacion': 'Muy alto',
                            'extension': 'Pdf',
                            'lenguaje': 'en',
                            'pais': 'Colombia',
                            'palabrasClaves': 'prueba',
                            'fecha': '',
                            'areaConocimiento': 4,
                            'categoria': '3',
                            'tipoRecurso': 'Audio',
                            'nivelEducativo': 'Pregrado',
                            'nombreArchivo': 'file',
                            'archivo': '/home/ubuntu/proyecto/files/file.docx',
                            'recursoWebsite': '',
                            'recursoEstructura': '',
                            'recursoVersion': '',
                            'recursoMomentoC': '',
                            'recursoCoordenadas': '',
                            'recursoIdentCatalogo': '',
                            'recursoEsquema': '',
                            'recursoCambios': '',
                            'recursoLenguajeDatos': '',
                            'entidadNombre': '',
                            'entidadRolAutor': '',
                            'tecnicaLocalizacion': '',
                            'tecnicaPrevisualizacion': '',
                            'tecnicaInstInstalacion': '',
                            'tecnicaOtrosReq': '',
                            'educativaInteractividad': '',
                            'educativaDensidadSem': '',
                            'educativaDes': '',
                            'educativaRangoEdad': '',
                            'educativaTiempoA': '',
                            'educativaDificultad': '',
                            'comentariosComent': '',
                            'cometariosDesc': '',
                            'comentariosFecha': '',
                            'cometariosRating': '',
                            'derechosRestricciones': '',
                            'derechosCosto': '',
                            'derechosDesc': '', }
      session.save()
      os.environ['EMAIL_DSPACE'] = 'jc.cerquera10@uniandes.edu.co'
      os.environ['PASSWORD_DSPACE'] = 'dspace'
      os.environ['DNS_DSPACE'] = 'ec2-54-86-20-91.compute-1.amazonaws.com'
      response = client.post('/confir/', {'confirmar': 'confirmar'})
      print(response.status_code)
      self.assertEqual(response.status_code, 302)
