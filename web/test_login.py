from django.test import TestCase
from django.test import Client
from views import *


__author__ = 'JAMIL MURCIA'


class TestLogin(TestCase):

# Tests para validar el url del login
    def test_urlLogin(self):
        url = reverse('login')
        self.assertEqual(url, '/login')

    # Tests para validar al hacer el Signup de un usuario este el formulario para ingresar los datos
    def test_htmlLogin(self):
        client = Client()
        response = client.get('/signup')
        self.assertContains(response,'tablaSignUp',1)

    def test_viewSignUp(self):
        client = Client()
        response = client.post('/signup',{'username':'jfmurcia',
                     'password':'123',
                     'email':'jfmurcia@gg.com',
                     'first_name':'Jamil',
                     'last_name':'Murcia',
                     'groups':'estudiante'})
        self.assertEqual(response.status_code,200,'Error en la vista')




