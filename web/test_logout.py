from django.test import TestCase
from django.core.urlresolvers import reverse

__author__ = 'JC.CERQUERA10@UNIANDES.EDU.CO'


class TestLogin(TestCase):
    # Tests para validar la url de logout
    def test_urlLogin(self):
        url = reverse('logout')
        self.assertEqual(url, '/logout')
