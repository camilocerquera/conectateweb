__author__ = 'root'
from web.models import Item
from rest_framework import serializers


# Serializar modelo de Item
class ItemSerialializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Item
        fields = ('id','name')
