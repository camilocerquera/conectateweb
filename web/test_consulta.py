__author__ = 'jc.cerquera10@uniandes.edu.co'
from django.test import TestCase
from models import RecursoForm5
from django.test import Client

'''Test Unitarios de las operaciones de consulta de recursos'''


class TestConsultaRecurso(TestCase):
    """
    Valida el formuladorio de ingreso de palabra clave
    """

    def test_formConsultaKeyword(self):
        form_data = {
            "palabraClave": "taxonomia",
            "areaConocimiento": 0
        }
        form = RecursoForm5(data=form_data)
        self.assertTrue(form.is_valid(), 'formulario busqueda palabra clave invalido')

    """
    Valida la funcionalidad de consulta de recurso educativo por palabra clave
    """

    def test_consultaKeyword(self):
        form_data = {
            "palabraClave": "taxonomia",
            "areaConocimiento": 0
        }
        client = Client()
        response = client.post("/", form_data)
        self.assertEqual(response.status_code, 200, 'Error consultando recurso por keyword')

    """
   Valida la funcionalidad de consulta de recurso educativo por palabra clave y en REDA
   """

    def test_consultaKeywordREDA(self):
        form_data = {
            "palabraClave": "taxonomia",
            "areaConocimiento": 0,
            "reda": True
        }
        client = Client()
        response = client.post("/", form_data)
        self.assertEqual(response.status_code, 200, 'Error consultando recurso en REDA por keyword')
