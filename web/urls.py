from django.conf.urls import url

from . import views
from django.contrib.auth.views import login, logout

urlpatterns = [
    url(r'^$', views.main, name='main'),
    url(r'^clear/$', views.clear, name='clear'),
    url(r'^basica/$', views.info_basica, name='info_basica'),
    url(r'^avanzada/$', views.info_avanzada, name='info_avanzada'),
    url(r'^carga/$', views.info_carga, name='info_carga'),
    url(r'^clasif/$', views.info_clasif, name='info_clasif'),
    url(r'^confir/$', views.info_confirm, name='info_confirm'),
    url(r'^signup$', views.signup, name='signup'),
    url(r'^login$', login, {'template_name': 'login.html', }, name="login"),
    url(r'^home$', views.home, name='home'),
    url(r'^logout$', logout, {'next_page': '../', }, name="logout"),
    url(r'^recurso/(?P<identificador>\w+)/$', views.ver_recurso, name='verRecurso'),
    url(r'^eliminarRecurso/(?P<identificador>\w+)/$', views.eliminarRecurso, name='eliminarRecurso'),
    url(r'^recursos/(?P<identificador>\w+)/$', views.recursos, name='recursos'),
]
