__author__ = 'root'

from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.forms.widgets import Select


class SignUpForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password', 'email', 'first_name', 'last_name','groups']
        widgets = {
            'password': forms.PasswordInput(),
        }
