# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bitstream',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('handle', models.CharField(max_length=500)),
                ('type', models.CharField(max_length=500)),
                ('expand', models.CharField(max_length=500, choices=[(b'parent', b'parent'), (b'policies', b'policies'), (b'all', b'all')])),
                ('bundleName', models.CharField(max_length=500)),
                ('description', models.TextField(default=b'')),
                ('format', models.CharField(max_length=500)),
                ('mimeType', models.CharField(max_length=500)),
                ('retrieveLink', models.CharField(max_length=500)),
                ('checkSum', models.CharField(max_length=500)),
                ('checkSumAlgorithm', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('handle', models.CharField(max_length=500)),
                ('type', models.CharField(max_length=500)),
                ('expand', models.CharField(max_length=500, choices=[(b'parentCommunityList', b'parentCommunityList'), (b'parentCommunity', b'parentCommunity'), (b'items', b'items'), (b'license', b'license'), (b'logo', b'logo'), (b'all', b'all')])),
                ('logo', models.CharField(max_length=500, null=True)),
                ('license', models.CharField(max_length=500, null=True)),
                ('copyrightText', models.CharField(default=b'', max_length=500)),
                ('introductoryText', models.CharField(default=b'', max_length=500)),
                ('shortDescription', models.TextField(default=b'', max_length=500)),
                ('sidebarText', models.CharField(default=b'', max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Community',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('handle', models.CharField(max_length=500)),
                ('type', models.CharField(max_length=500)),
                ('link', models.CharField(max_length=500)),
                ('expand', models.CharField(max_length=500, choices=[(b'parentCommunity', b'parentCommunity'), (b'collections', b'collections'), (b'subCommunities', b'subCommunities'), (b'logo', b'logo'), (b'all', b'all')])),
                ('logo', models.CharField(max_length=500, null=True)),
                ('copyrightText', models.CharField(default=b'', max_length=500)),
                ('introductoryText', models.CharField(default=b'', max_length=500)),
                ('shortDescription', models.TextField(default=b'', max_length=500)),
                ('sidebarText', models.CharField(default=b'', max_length=500)),
                ('parentCommunity', models.ForeignKey(to='web.Community')),
                ('subcommunities', models.ManyToManyField(related_name='subcommunities_rel_+', to='web.Community')),
            ],
        ),
        migrations.CreateModel(
            name='DItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha', models.CharField(max_length=10)),
                ('titulo', models.CharField(max_length=20)),
                ('autor', models.CharField(max_length=20)),
                ('descripcion', models.CharField(max_length=50)),
                ('pais', models.CharField(max_length=20)),
                ('keywords', models.CharField(max_length=20)),
                ('idItem', models.IntegerField(default=0)),
                ('reda', models.CharField(default=b'N', max_length=1)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='MetadataEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=500)),
                ('value', models.CharField(max_length=500)),
                ('language', models.CharField(max_length=500)),
                ('parentObject', models.ForeignKey(to='web.Item')),
            ],
        ),
        migrations.CreateModel(
            name='MetadataItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=2048)),
                ('descripcion', models.CharField(max_length=2048)),
                ('autor', models.CharField(max_length=2048)),
                ('nivelAgregacion', models.CharField(max_length=10)),
                ('extension', models.CharField(max_length=10)),
                ('lenguaje', models.CharField(max_length=100)),
                ('pais', models.CharField(max_length=100)),
                ('palabrasClaves', models.CharField(max_length=2048)),
                ('fecha', models.CharField(max_length=100)),
                ('areaConocimiento', models.CharField(max_length=300)),
                ('categoria', models.CharField(max_length=300)),
                ('tipoRecurso', models.CharField(max_length=100)),
                ('nivelEducativo', models.CharField(max_length=100)),
                ('recursoWebsite', models.CharField(max_length=2048)),
                ('recursoEstructura', models.CharField(max_length=2048)),
                ('recursoVersion', models.CharField(max_length=2048)),
                ('recursoMomentoC', models.CharField(max_length=2048)),
                ('recursoCoordenadas', models.CharField(max_length=2048)),
                ('recursoIdentCatalogo', models.CharField(max_length=2048)),
                ('recursoEsquema', models.CharField(max_length=2048)),
                ('recursoCambios', models.CharField(max_length=2048)),
                ('recursoLenguajeDatos', models.CharField(max_length=2048)),
                ('entidadNombre', models.CharField(max_length=2048)),
                ('tecnicaPrevisualizacion', models.CharField(max_length=2048)),
                ('tecnicaInstInstalacion', models.CharField(max_length=2048)),
                ('tecnicaOtrosReq', models.CharField(max_length=2048)),
                ('educativaInteractividad', models.CharField(max_length=100)),
                ('educativaDensidadSem', models.CharField(max_length=100)),
                ('educativaDes', models.CharField(max_length=2048)),
                ('educativaRangoEdad', models.CharField(max_length=2048)),
                ('educativaTiempoA', models.CharField(max_length=2048)),
                ('educativaDificultad', models.CharField(max_length=2048)),
                ('comentariosComent', models.CharField(max_length=2048)),
                ('cometariosDesc', models.CharField(max_length=2048)),
                ('comentariosFecha', models.CharField(max_length=2048)),
                ('cometariosRating', models.CharField(max_length=2048)),
                ('derechosRestricciones', models.CharField(max_length=2048)),
                ('derechosCosto', models.CharField(max_length=2048)),
                ('derechosDesc', models.CharField(max_length=2048)),
            ],
        ),
        migrations.CreateModel(
            name='ResourcePolicy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('action', models.CharField(max_length=500)),
                ('resourceType', models.CharField(max_length=500)),
                ('rpDescription', models.CharField(max_length=500, null=True)),
                ('rpName', models.CharField(max_length=500, null=True)),
                ('rpType', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fullname', models.CharField(max_length=500)),
                ('token', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.CharField(max_length=500)),
                ('password', models.CharField(max_length=500)),
            ],
        ),
        migrations.AddField(
            model_name='collection',
            name='parentCommunity',
            field=models.ForeignKey(related_name='community_fk', to='web.Community', null=True),
        ),
        migrations.AddField(
            model_name='collection',
            name='parentCommunityList',
            field=models.ManyToManyField(to='web.Community'),
        ),
        migrations.AddField(
            model_name='bitstream',
            name='parentObject',
            field=models.ForeignKey(to='web.Item'),
        ),
        migrations.AddField(
            model_name='bitstream',
            name='policies',
            field=models.ForeignKey(to='web.ResourcePolicy'),
        ),
    ]
