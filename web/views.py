#vim: set fileencoding=utf-8:
from django.shortcuts import render_to_response,HttpResponseRedirect
from web.models import RecursoForm1, RecursoForm2,RecursoForm3, RecursoForm4, RecursoForm5, DItem, MetadataItem
import requests
from serializers import ItemSerialializer
from django.conf import settings
import os
from django.template import RequestContext
from forms import SignUpForm
from django.contrib.auth.models import User,Group
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json

@login_required(login_url='../login')
def clear(request):
    request.session['recurso'] = {}
    return HttpResponseRedirect("/basica/")

# Se define la funcionalidad de la pestaña Ingresar informacion basica de recurso educativo
@login_required()
def info_basica(request):

    # Se valida si se ha presionado el botón continuar
    if request.method=='POST':
        # Se carga la información ingresada en pantalla
        form=RecursoForm1(request.POST)
        # Se valida si la información ingresada es correcta,
        # es decir si estan todos los campos diligenciados
        if form.is_valid():
            titulo=form.cleaned_data['titulo']
            descripcion=form.cleaned_data['descripcion']
            autor=form.cleaned_data['autor']
            nivelAgregacion=form.cleaned_data['nivelAgregacion']
            extension=form.cleaned_data['extension']
            lenguaje=form.cleaned_data['lenguaje']
            pais=form.cleaned_data['pais']
            palabrasClaves=form.cleaned_data['palabrasClaves']
            fecha=form.cleaned_data['fecha'].strftime("%Y-%m-%d %H:%M:%S")
            # Se guarda la información temporalmente
            recurso=request.session.get('recurso')
            recurso['titulo'] = titulo
            recurso['descripcion'] = descripcion
            recurso['autor'] = autor
            recurso['nivelAgregacion'] = nivelAgregacion
            recurso['extension'] = extension
            recurso['lenguaje'] = lenguaje
            recurso['pais'] = pais
            recurso['palabrasClaves'] = palabrasClaves
            recurso['fecha'] = fecha
            request.session['recurso'] =recurso
             # Si toda la información ha sido ingresada,
            # se pasa a la pestaña de carga de recurso educativo
            tab_activa=1
            return HttpResponseRedirect("/avanzada/")
        else:
            # Si la información es incompleta, se continua en la misma pestaña
            tab_activa=0

    # Si es el primer ingreso a la pantalla
    else:
        try:
            recurso=request.session.get('recurso')
            form=RecursoForm1(initial={ 'titulo':recurso['titulo'],
                                        'descripcion' : recurso['descripcion'],
                                        'autor' : request.user,
                                        'nivelAgreagacion' : recurso['nivelAgregacion'],
                                        'extension' : recurso['extension'],
                                        'lenguaje' : recurso['lenguaje'],
                                        'pais':recurso['pais'],
                                        'palabrasClaves' : recurso['palabrasClaves']
                                       })
        except:
                form=RecursoForm1(initial={ 'autor' : request.user})
        # Se abra la tab inicial de ingresar recurso educativo
        tab_activa=0

    return render_to_response('catalogar_recurso.html',{'form':form,'request':request,'tab_activa':tab_activa},context_instance=RequestContext(request))



# Se define la funcionalidad de la pestaña Ingresar informacion avanzada de recurso educativo
def info_avanzada(request):

       # Se valida si se ha presionado el botón continuar
    if request.method=='POST':

        # Se carga la información ingresada en pantalla
        form=RecursoForm4(request.POST)
        # Se valida si la información ingresada es correcta,
        # es decir si estan todos los campos diligenciados
        if form.is_valid():
            recursoWebsite=form.cleaned_data['recursoWebsite']
            recursoEstructura=form.cleaned_data['recursoEstructura']
            recursoVersion=form.cleaned_data['recursoVersion']
            recursoMomentoC=form.cleaned_data['recursoMomentoC']
            recursoCoordenadas=form.cleaned_data['recursoCoordenadas']
            recursoIdentCatalogo=form.cleaned_data['recursoIdentCatalogo']
            recursoEsquema=form.cleaned_data['recursoEsquema']
            recursoCambios=form.cleaned_data['recursoCambios']
            recursoLenguajeDatos=form.cleaned_data['recursoLenguajeDatos']
            entidadNombre=form.cleaned_data['entidadNombre']
            tecnicaLocalizacion=form.cleaned_data['tecnicaLocalizacion']
            tecnicaPrevisualizacion=form.cleaned_data['tecnicaPrevisualizacion']
            tecnicaInstInstalacion=form.cleaned_data['tecnicaInstInstalacion']
            tecnicaOtrosReq=form.cleaned_data['tecnicaOtrosReq']
            educativaInteractividad=form.cleaned_data['educativaInteractividad']
            educativaDensidadSem=form.cleaned_data['educativaDensidadSem']
            educativaDes=form.cleaned_data['educativaDes']
            educativaRangoEdad=form.cleaned_data['educativaRangoEdad']
            educativaTiempoA=form.cleaned_data['educativaTiempoA']
            educativaDificultad=form.cleaned_data['educativaDificultad']
            comentariosComent=form.cleaned_data['comentariosComent']
            cometariosDesc=form.cleaned_data['cometariosDesc']

            if form.cleaned_data['comentariosFecha']!= None:
                comentariosFecha=form.cleaned_data['comentariosFecha'].strftime("%Y-%m-%d %H:%M:%S")
            else:
                comentariosFecha=form.cleaned_data['comentariosFecha']

            cometariosRating=form.cleaned_data['cometariosRating']
            derechosRestricciones=form.cleaned_data['derechosRestricciones']
            derechosCosto=form.cleaned_data['derechosCosto']
            derechosDesc=form.cleaned_data['derechosDesc']

            # Se carga el recurso de sesion
            recurso=request.session.get('recurso')
            # Se setena los datos al diccionario
            recurso['recursoWebsite'] = recursoWebsite
            recurso['recursoEstructura'] = recursoEstructura
            recurso['recursoVersion'] = recursoVersion
            recurso['recursoMomentoC'] = recursoMomentoC
            recurso['recursoCoordenadas'] = recursoCoordenadas
            recurso['recursoIdentCatalogo'] = recursoIdentCatalogo
            recurso['recursoEsquema'] = recursoEsquema
            recurso['recursoCambios'] = recursoCambios
            recurso['recursoLenguajeDatos'] = recursoLenguajeDatos
            recurso['entidadNombre'] = entidadNombre
            recurso['tecnicaPrevisualizacion'] = tecnicaPrevisualizacion
            recurso['tecnicaInstInstalacion'] = tecnicaInstInstalacion
            recurso['tecnicaOtrosReq'] = tecnicaOtrosReq
            recurso['educativaInteractividad'] = educativaInteractividad
            recurso['educativaDensidadSem'] = educativaDensidadSem
            recurso['educativaDes'] = educativaDes
            recurso['educativaRangoEdad'] = educativaRangoEdad
            recurso['educativaTiempoA'] = educativaTiempoA
            recurso['educativaDificultad'] = educativaDificultad
            recurso['comentariosComent'] = comentariosComent
            recurso['cometariosDesc'] = cometariosDesc
            recurso['comentariosFecha'] = comentariosFecha
            recurso['cometariosRating'] = cometariosRating
            recurso['derechosRestricciones'] = derechosRestricciones
            recurso['derechosCosto'] = derechosCosto
            recurso['derechosDesc'] = derechosDesc

            # se guarda la informacion del recurso
            request.session['recurso'] =recurso

            # Si toda la información ha sido ingresada,
            # se pasa a la pestaña de carga de recurso

            tab_activa=2
            return HttpResponseRedirect("/carga/")
        else:
            # Si la información es incompleta, se continua en la misma pestaña
            tab_activa=1

    # Si es el primer ingreso a la pantalla
    else:
        try:
            recurso=request.session.get('recurso')
            form=RecursoForm4(initial={ 'recursoWebsite': recurso['recursoWebsite'],
                                        'recursoEstructura': recurso['recursoEstructura'],
                                        'recursoVersion': recurso['recursoVersion'],
                                        'recursoMomentoC': recurso['recursoMomentoC'],
                                        'recursoCoordenadas': recurso['recursoCoordenadas'],
                                        'recursoIdentCatalogo': recurso['recursoIdentCatalogo'],
                                        'recursoEsquema': recurso['recursoEsquema'],
                                        'recursoCambios': recurso['recursoCambios'],
                                        'recursoLenguajeDatos': recurso['recursoLenguajeDatos'],
                                        'entidadNombre': recurso['entidadNombre'],
                                        'tecnicaPrevisualizacion': recurso['tecnicaPrevisualizacion'],
                                        'tecnicaInstInstalacion': recurso['tecnicaInstInstalacion'],
                                        'tecnicaOtrosReq': recurso['tecnicaOtrosReq'],
                                        'educativaInteractividad': recurso['educativaInteractividad'],
                                        'educativaDensidadSem': recurso['educativaDensidadSem'],
                                        'educativaDes': recurso['educativaDes'],
                                        'educativaRangoEdad': recurso['educativaRangoEdad'],
                                        'educativaTiempoA': recurso['educativaTiempoA'],
                                        'educativaDificultad': recurso['educativaDificultad'],
                                        'comentariosComent': recurso['comentariosComent'],
                                        'cometariosDesc': recurso['cometariosDesc'],
                                        'comentariosFecha': recurso['comentariosFecha'],
                                        'cometariosRating': recurso['cometariosRating'],
                                        'derechosRestricciones': recurso['derechosRestricciones'],
                                        'derechosCosto': recurso['derechosCosto'],
                                        'derechosDesc': recurso['derechosDesc'],
                                       })
        except:
            form=RecursoForm4
        # Se abra la tab inicial de ingresar recurso educativo
        tab_activa=1

    return render_to_response('catalogar_recurso.html',{'form':form,'request':request,'tab_activa':tab_activa},context_instance=RequestContext(request))



@login_required()
def info_carga(request):
    # Se valida si se ha presionado el botón continuar
    if request.method=='POST':
        # Se carga la información ingresada en pantalla
        form=RecursoForm2(request.POST,request.FILES)
        # Se valida si la información ingresada es correcta,
        # es decir si estan todos los campos diligenciados
        if form.is_valid():
            # Se guarda la información ingresada temporalmente
            file=form.cleaned_data['recurso']
            destination = open(settings.MEDIA_ROOT + '/temp/' +file._get_name(), 'wb+')

            for chunk in file.chunks():
                    destination.write(chunk)
            destination.close()
            recurso=request.session.get('recurso')
            recurso['archivo'] = settings.MEDIA_ROOT + '/temp/' +file._get_name()
            recurso['nombreArchivo'] = file._get_name()
            request.session['recurso'] =recurso
            # Si toda la información ha sido ingresada,
            # Se pasa a la pestaña de carga de recurso educativo
            tab_activa=3
            return HttpResponseRedirect("/clasif/")

        else:
            # Si la información es incompleta, se continua en la misma pestaña
            tab_activa=2
    # Si es el primer ingreso a la pantalla
    else:
        # Se carga el formulario limpio en pantalla
        form=RecursoForm2()
        # Se abra la tab inicial de ingresar recurso educativo
        tab_activa=2
    return render_to_response('catalogar_recurso.html',{'form':form,'request':request,'tab_activa':tab_activa},context_instance=RequestContext(request))

@login_required()
def info_clasif(request):
    # Se valida si se ha presionado el botón continuar
    if request.method=='POST':
        # Se carga la información ingresada en pantalla
        form=RecursoForm3(request.POST)
        # Se valida si la información ingresada es correcta,
        # es decir si estan todos los campos diligenciados
        if form.is_valid():
            # Se guarda la información ingresada temporalmente

            recurso=request.session.get('recurso')
            #recurso['area'] = 0
            recurso['categoria'] = form.cleaned_data['area']
            recurso['tipoRecurso'] = form.cleaned_data['tipoRecurso']
            recurso['nivelEducativo'] = form.cleaned_data['nivelEducativo']
            request.session['recurso'] =recurso

            # Si toda la información ha sido ingresada,
            # se pasa a la pestaña de clasificaciòn de recurso educativo
            tab_activa=4
            return HttpResponseRedirect("/confir/")
        else:
            # Si la información es incompleta, se continua en la misma pestaña
            tab_activa=3
    # Si es el primer ingreso a la pantalla
    else:
        try:
            recurso=request.session.get('recurso')
            form=RecursoForm3(initial={ 'categoria':recurso['categoria'],
                                        'tipoRecurso' : recurso['tipoRecurso'],
                                        'nivelEducativo' : recurso['nivelEducativo']
                                       })
        except:
            #Obtiene id del Area de Conocimiento seleccionado
            #IdArea = obtieneIdArea(request)
            # Se carga el formulario limpio en pantalla
            form = RecursoForm3()
            #form = RecursoForm3(IdArea=IdArea)
        # Se abra la tab inicial de ingresar recurso educativo
        tab_activa=3

    return render_to_response('catalogar_recurso.html',{'form':form,'request':request,'tab_activa':tab_activa},context_instance=RequestContext(request))

@login_required()
def info_confirm(request):
    msj = ""
    # Se valida si se ha presionado el botón continuar
    if request.method=='POST':
        if request.POST.get('confirmar'):
            confirmar(request)
            msj = "El recurso educativo fue catalogado exitosamente"
        elif request.POST.get('cancelar'):
           return HttpResponseRedirect("/clear/")
    tab_activa =4
    recurso=request.session.get('recurso')

    return render_to_response('catalogar_recurso.html',{'request':request,'msj':msj,'tab_activa':tab_activa,'recurso':recurso},context_instance=RequestContext(request))

# Se define la funcionalidad de la pestaña de confirmar recurso educativo
@login_required()
def confirmar(request):

    msj = ""

    #Se obtiene el objeto y todos los campos ingresados
    recurso=request.session.get('recurso')

    # Se loguea en dspace
    parametros = {
       "email": os.environ.get('EMAIL_DSPACE'),
       "password": os.environ.get('PASSWORD_DSPACE')
        }
    r = requests.post("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/login", json=parametros)
    print(r.status_code, r.text)
    token = r.text.strip("(")
    token = r.text.strip(")")

    # Verifica el estado del usuario
    headers = {"rest-dspace-token": token,
                   "content-type": "application/json",
                   "Accept": "application/json"}
    r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/status", headers=headers)
    print(r.status_code, r.text)

    # Add item
    titulo = recurso['titulo']
    descripcion = recurso['descripcion']
    autor = recurso['autor']
    nivelAgreagacion = recurso['nivelAgregacion']
    extension = recurso['extension']
    lenguaje = recurso['lenguaje']
    pais = recurso['pais']
    palabrasClaves = recurso['palabrasClaves']
    fecha = recurso['fecha']
    #areaConocimiento = recurso['areaConocimiento']
    categoria = recurso['categoria']
    tipoRecurso = recurso['tipoRecurso']
    nivelEducativo = recurso['nivelEducativo']

    #Se recupera la información avanzada del recurso educativo
    recursoWebsite = recurso['recursoWebsite']
    recursoEstructura = recurso['recursoEstructura']
    recursoVersion = recurso['recursoVersion']
    recursoMomentoC = recurso['recursoMomentoC']
    recursoCoordenadas = recurso['recursoCoordenadas']
    recursoIdentCatalogo = recurso['recursoIdentCatalogo']
    recursoEsquema = recurso['recursoEsquema']
    recursoCambios = recurso['recursoCambios']
    recursoLenguajeDatos = recurso['recursoLenguajeDatos']
    entidadNombre = recurso['entidadNombre']
    tecnicaPrevisualizacion = recurso['tecnicaPrevisualizacion']
    tecnicaInstInstalacion = recurso['tecnicaInstInstalacion']
    tecnicaOtrosReq = recurso['tecnicaOtrosReq']
    educativaInteractividad = recurso['educativaInteractividad']
    educativaDensidadSem = recurso['educativaDensidadSem']
    educativaDes = recurso['educativaDes']
    educativaRangoEdad = recurso['educativaRangoEdad']
    educativaTiempoA = recurso['educativaTiempoA']
    educativaDificultad = recurso['educativaDificultad']
    comentariosComent = recurso['comentariosComent']
    cometariosDesc = recurso['cometariosDesc']
    comentariosFecha = recurso['comentariosFecha']
    cometariosRating = recurso['cometariosRating']
    derechosRestricciones = recurso['derechosRestricciones']
    derechosCosto = recurso['derechosCosto']
    derechosDesc = recurso['derechosDesc']


    parametros = {
        "metadata": [
            #Informacion basica
            {"key": "lom.general.title", "value": titulo},
            {"key": "lom.general.description", "value": descripcion},
            {"key": "lom.lifecycle.contribute-role", "value": autor},
            {"key": "lom.technical.format", "value": extension},
            {"key": "lom.general.language", "value": lenguaje},
            {"key": "lom.technical.location", "value": pais},
            {"key": "lom.general.keyword", "value": palabrasClaves},
            {"key": "lom.lifecycle.contribute-date", "value": fecha},
            {"key": "lom.technical.requirement-orcomponent-type", "value": tipoRecurso},
            {"key": "lom.general.aggregationlevel", "value": nivelAgreagacion},
            {"key": "lom.educational.level", "value": nivelEducativo},
            #Informacion avanzada
            {"key": "lom.general.coverage-netsites", "value": recursoWebsite},
            {"key": "lom.general.structure", "value": recursoEstructura},
            {"key": "lom.lifecycle.version", "value": recursoVersion},
            {"key": "lom.general.coverage-contextualmoment", "value": recursoMomentoC},
            {"key": "lom.general.coverage-geolocationcoordinates", "value": recursoCoordenadas},
            {"key": "lom.metadata.identifier-catalog", "value": recursoIdentCatalogo},
            {"key": "lom.metadata.scheme", "value": recursoEsquema},
            {"key": "lom.lifecycle.changelog", "value": recursoCambios},
            {"key": "lom.metadata.language", "value": recursoLenguajeDatos},
            #Informacion avanzada entidad
            {"key": "lom.lifecycle.contribute-entity", "value": entidadNombre},
            #Informacion avanzada tecnica
            {"key": "lom.technical.installationinstructions", "value": tecnicaInstInstalacion},
            {"key": "lom.technical.installationinstructions-otherrequirements", "value": tecnicaOtrosReq},
            {"key": "lom.technical.previews", "value": tecnicaPrevisualizacion},
            #Informacion avanzada educativo
            {"key": "lom.educational.typeofinteractivity", "value": educativaInteractividad},
            {"key": "lom.educational.semanticdensity", "value": educativaDensidadSem},
            {"key": "lom.educational.description", "value": educativaDes},
            {"key": "lom.educational.agerange", "value": educativaRangoEdad},
            {"key": "lom.educational.timelearning", "value": educativaTiempoA},
            {"key": "lom.educational.dificult", "value": educativaDificultad},
            #Informacion avanzada comentarios
            {"key": "lom.annotation.entity", "value":comentariosComent},
            {"key": "lom.annotation.date", "value": comentariosFecha},
            {"key": "lom.annotation.description", "value": cometariosDesc},
            {"key": "lom.annotation.setrating", "value": cometariosRating},
            #Informacion avanzada Derechos de autor
            {"key": "lom.rights.copyrightandotherrestrictions", "value": derechosRestricciones},
            {"key": "lom.rights.cost", "value": derechosCosto},
            {"key": "lom.rights.description", "value": derechosDesc}
        ]}

    r = requests.post("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/collections/" + categoria + "/items", json=parametros,
                      headers=headers)

    print(r.status_code, r.json())
    text = r.text
    datos = text.split(',')
    textId = datos[0]
    datosId = textId.split(':')
    id = datosId[1]
    data = r.json()
    serializer = ItemSerialializer(data = data)
    if serializer.is_valid():
        item = serializer.data

    # Add bitstream

    headers2 = {"rest-dspace-token": token,
                "content-type": "application/json"}

    itemId = id
    #print("ITEM_ID: "+itemId)
    nombreArchivo = recurso['nombreArchivo']
    descripcion = "bitstream del item"
    rutaArchivo = recurso['archivo']
    #print("NOMBRE_ARCHIVO:  "+nombreArchivo)
    #print("RUTA_ARCHIVO: "+rutaArchivo)

    files = {'file': open(rutaArchivo, 'rb')}
    parametros = {
        "name": nombreArchivo,
        "description": descripcion}
    r = requests.post("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items/" + itemId + "/bitstreams", files=files, json=parametros,
                      headers=headers2)
    print(r.status_code, r.text)


    # Logout usuario dspace
    r = requests.post("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/logout", headers=headers)
    print(r.status_code, r.text)

    msj = "El recurso educativo fue catalogado exitosamente"

# Vista principal de página
def main(request):
    if request.method=='POST':
        # Se carga la información ingresada en pantalla
        form=RecursoForm5(request.POST)
        # Se valida si la información ingresada es correcta,
        # es decir si estan todos los campos diligenciados
        if form.is_valid():
            # Se guarda la información ingresada temporalmente
            palabraClave=form.cleaned_data['palabraClave']
            areaConocimiento=form.cleaned_data['areaConocimiento']
            reda = form.cleaned_data['reda']
            ItemList = buscar(palabraClave, areaConocimiento, reda)

            # Se incluye paginacion
            paginator = Paginator(ItemList, 4) # Muestra 5 recursos
            page = request.GET.get('page')
            try:
                Items = paginator.page(page)
            except PageNotAnInteger:
                # si la pagina no es un entero muestra la pagina 1
                Items = paginator.page(1)
            except EmptyPage:
                # Si la pagina esta fuera del rango (e.g. 9999),Muestra la ultima pagina de resultados
                Items = paginator.page(paginator.num_pages)
            # Si toda la información ha sido ingresada,
            # se pasa a la pestaña de clasificaciòn de recurso educativo
            return render_to_response('main.html', {'form':form,'request':request,'Items':Items},context_instance=RequestContext(request))

    # Si es el primer ingreso a la pantalla
    else:
        # Se carga el formulario limpio en pantalla
        form = RecursoForm5()
        ItemList = consultaItems()

        # Se incluye paginacion
        paginator = Paginator(ItemList, 4) # Muestra 5 recursos
        page = request.GET.get('page')
        try:
            Items = paginator.page(page)
        except PageNotAnInteger:
            # si la pagina no es un entero muestra la pagina 1
            Items = paginator.page(1)
        except EmptyPage:
            # Si la pagina esta fuera del rango (e.g. 9999),Muestra la ultima pagina de resultados
            Items = paginator.page(paginator.num_pages)

        # Si toda la información ha sido ingresada,
        # se pasa a la pestaña de clasificaciòn de recurso educativo
    return render_to_response('main.html', {'form':form,'request':request,'Items':Items},context_instance=RequestContext(request))


# Vista para crear un nuevo usuario
def signup(request):
    if request.method == 'POST':  # If the form has been submitted...
        form = SignUpForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass

            # Process the data in form.cleaned_data
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            email = form.cleaned_data["email"]
            first_name = form.cleaned_data["first_name"]
            last_name = form.cleaned_data["last_name"]

            # At this point, user is a User object that has already been saved
            # to the database. You can continue to change its attributes
            # if you want to change other fields.
            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.groups.add(Group.objects.get(id=form.cleaned_data["groups"]))

            # Save new user attributes
            user.save()

            return HttpResponseRedirect(reverse('main'))  # Redirect after POST
    else:
        form = SignUpForm()

    data = {
        'form': form,
    }
    return render_to_response('signup.html', data, context_instance=RequestContext(request))


@login_required()
def home(request):
    return render_to_response('home.html', {'user': request.user}, context_instance=RequestContext(request))

def obtieneIdArea(request):
    datos=request.session.get('recurso')
    for dato in datos:
        if dato == "areaConocimiento":
            IdArea = datos['areaConocimiento']
        else:
            IdArea = ""
    return IdArea

def consultaItems():
    IndId1 = 0
    ItemId = 0
    #Obtiene la información de todos los items
    r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items")
    #r = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/items")
    r1 = r.text.replace(":",",")
    r2 = r1.replace('"',"")
    r3 = r2.replace("{","")
    r4 = r3.replace("[","")
    datos = r4.split(',')
    Items=[]
    for dato in datos:
        if (IndId1==1):
            ItemId = dato
            IndId1 = 2

        if dato=="id":
            IndId1 = 1
        #Obtiene la información para cada uno de los items encontrados y los filtra por palabra clave
        if (IndId1==2):
            IndTitulo = 0
            ItemTitulo = ""
            IndAutor = 0
            ItemAutor = ""
            IndDesc = 0
            ItemDesc = ""
            IndFecha = 0
            ItemFecha = ""
            IndPais = 0
            ItemPais = ""
            IndKey = 0
            ItemKey = ""
            t = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items/" + ItemId + "/metadata")
            #t = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/items/" + ItemId + "/metadata")
            t1 = t.text.replace(":",",")
            t2 = t1.replace('"',"")
            t3 = t2.replace("{","")
            t4 = t3.replace("[","")
            t5 = t4.replace("}","")
            t6 = t5.replace("]","")
            t7 = t6.replace("key,","")
            t8 = t7.replace("value,","")
            metadatas = t8.split(',')
            for metadata in metadatas:
                if (IndTitulo==1):
                    ItemTitulo = metadata
                    IndTitulo = 2
                if IndAutor == 1:
                    ItemAutor = metadata
                    IndAutor = 2
                if IndDesc == 1:
                    ItemDesc = metadata
                    IndDesc = 2
                if IndFecha == 1:
                    fecha = metadata.split('T')
                    ItemFecha = fecha[0]
                    IndFecha = 2
                if IndPais == 1:
                    ItemPais = metadata
                    IndPais = 2
                if IndKey == 1:
                    ItemKey = metadata
                    IndKey = 2

                if metadata=="lom.general.title":
                    IndTitulo = 1
                if metadata == "lom.lifecycle.contribute-role":
                    IndAutor = 1
                if metadata == "lom.general.description":
                    IndDesc = 1
                if metadata == "dc.date.available":
                    IndFecha = 1
                if metadata == "lom.technical.location":
                    IndPais = 1
                if metadata == "lom.general.keyword":
                    IndKey = 1

                if (IndTitulo==2 and IndAutor==2 and IndDesc==2 and IndFecha==2 and IndPais==2 and IndKey==2):
                    Items.append(DItem(idItem = ItemId, fecha = ItemFecha, titulo = ItemTitulo, autor = ItemAutor, descripcion = ItemDesc, pais = ItemPais, keywords = ItemKey))
                    IndTitulo = 0
                    IndAutor = 0
                    IndDesc = 0
                    IndFecha = 0
                    IndPais = 0
                    IndKey = 0
                    ItemTitulo = ""
                    ItemAutor = ""
                    ItemDesc = ""
                    ItemFecha = ""
                    ItemPais = ""
                    ItemKey = ""

            IndId1 = 0
            ItemId = 0
    #Items = sorted(Items, cmp=None, key=DItem.fecha, reverse=True)
    return Items

def buscar(palabraClave, areaConocimiento, reda):
    pKey = palabraClave
    pArea = areaConocimiento
    print(pKey)
    print(pArea)
    Items = []
    if pKey != "" and pArea == "0":
        print(1)
        Items = consultaItemsClave(pKey, reda)
    else:
        print(2)
        if pArea != "0":
            Items = consultaItemsArea(pKey, pArea)
        else:
            Items = consultaItems()
    print(Items)
    return Items

def consultaItemsClave(palabraClave, reda):
    pKey = palabraClave
    IndId1 = 0
    ItemId = 0
    #Obtiene la información de todos los items
    r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items")
    #r = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/items")
    r1 = r.text.replace(":",",")
    r2 = r1.replace('"',"")
    r3 = r2.replace("{","")
    r4 = r3.replace("[","")
    datos = r4.split(',')
    Items=[]
    for dato in datos:
        if (IndId1==1):
            ItemId = dato
            IndId1 = 2

        if dato=="id":
            IndId1 = 1
        #Obtiene la información para cada uno de los items encontrados y los filtra por palabra clave
        if (IndId1==2):
            IndTitulo = 0
            ItemTiAreatulo = ""
            IndAutor = 0
            ItemAutor = ""
            IndDesc = 0
            ItemDesc = ""
            IndFecha = 0
            ItemFecha = ""
            IndPais = 0
            ItemPais = ""
            IndKey = 0
            ItemKey = ""
            t = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items/" + ItemId + "/metadata")
            #t = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/items/" + ItemId + "/metadata")
            t1 = t.text.replace(":",",")
            t2 = t1.replace('"',"")
            t3 = t2.replace("{","")
            t4 = t3.replace("[","")
            t5 = t4.replace("}","")
            t6 = t5.replace("]","")
            t7 = t6.replace("key,","")
            t8 = t7.replace("value,","")
            metadatas = t8.split(',')
            for metadata in metadatas:
                if (IndTitulo==1):
                    ItemTitulo = metadata
                    IndTitulo = 2
                if IndAutor == 1:
                    ItemAutor = metadata
                    IndAutor = 2
                if IndDesc == 1:
                    ItemDesc = metadata
                    IndDesc = 2
                if IndFecha == 1:
                    fecha = metadata.split('T')
                    ItemFecha = fecha[0]
                    IndFecha = 2
                if IndPais == 1:
                    ItemPais = metadata
                    IndPais = 2
                if IndKey == 1:
                    ItemKey = metadata
                    IndKey = 2

                if metadata=="lom.general.title":
                    IndTitulo = 1
                if metadata == "lom.lifecycle.contribute-role":
                    IndAutor = 1
                if metadata == "lom.general.description":
                    IndDesc = 1
                if metadata == "dc.date.available":
                    IndFecha = 1
                if metadata == "lom.technical.location":
                    IndPais = 1
                if metadata == "lom.general.keyword":
                    IndKey = 1

                if (IndTitulo==2 and IndAutor==2 and IndDesc==2 and IndFecha==2 and IndPais==2 and IndKey==2):
                    if pKey == ItemKey:
                        Items.append(DItem(idItem = ItemId,titulo = ItemTitulo, autor = ItemAutor, descripcion = ItemDesc, pais = ItemPais, fecha = ItemFecha, keywords = ItemKey))
                    IndTitulo = 0
                    IndAutor = 0
                    IndDesc = 0
                    IndFecha = 0
                    IndPais = 0
                    IndKey = 0
                    ItemTitulo = ""
                    ItemAutor = ""
                    ItemDesc = ""
                    ItemFecha = ""
                    ItemPais = ""
                    ItemKey = ""

            IndId1 = 0
            ItemId = 0
    if reda == True:
        headers = {
            "content-type": "application/json",
            "Accept": "application/json"}
        response = requests.get("http://conectateservice.herokuapp.com/items/" + palabraClave + "/keyword/",headers=headers)
        text = response.text
        parsed_json = json.loads(text)
        print(parsed_json.__len__())
        print("test")
        for item in parsed_json:
            titulo = item['name']
            autor = item['autor']
            descripcion = item['descripcion']
            pais = item['pais']
            fecha = item['fecha']
            keyword = item['keyword']
            Items.append(DItem(titulo=titulo, autor=autor, descripcion=descripcion, pais=pais, fecha=fecha,
                               keywords=keyword, reda = 'S'))

    return Items


def consultaItemsArea(palabraClave, areaConocimiento):
    pKey = palabraClave
    IndId2 = 0
    CollectionId = 0
    #Obtiene la información de todas las collecciones asociadas a la comunidad
    r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/communities/" + areaConocimiento + "/collections")
    #r = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/communities/" + areaConocimiento + "/collections")
    r1 = r.text.replace(":",",")
    r2 = r1.replace('"',"")
    r3 = r2.replace("{","")
    r4 = r3.replace("[","")
    datos = r4.split(',')
    Items=[]
    for dato in datos:
        if (IndId2==1):
            CollectionId = dato
            IndId2 = 2

        if dato=="id":
            IndId2 = 1
        #Obtiene la información de todos los items asociados a la coleccion
        if (IndId2==2):
            IndId1 = 0
            ItemId = 0
            #Obtiene la información de todas las collecciones asociadas a la comunidad
            s = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/collections/" + CollectionId + "/items")
            #s = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/collections/" + CollectionId + "/items")
            s1 = s.text.replace(":",",")
            s2 = s1.replace('"',"")
            s3 = s2.replace("{","")
            s4 = s3.replace("[","")
            datosItems = s4.split(',')
            for datoItem in datosItems:
                if (IndId1==1):
                    ItemId = datoItem
                    IndId1 = 2

                if datoItem=="id":
                    IndId1 = 1
                #Obtiene la información para cada uno de los items encontrados
                if (IndId1==2):
                    IndTitulo = 0
                    ItemTitulo = ""
                    IndAutor = 0
                    ItemAutor = ""
                    IndDesc = 0
                    ItemDesc = ""
                    IndFecha = 0
                    ItemFecha = ""
                    IndPais = 0
                    ItemPais = ""
                    IndKey = 0
                    ItemKey = ""
                    t = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items/" + ItemId + "/metadata")
                    #t = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/items/" + ItemId + "/metadata")
                    t1 = t.text.replace(":",",")
                    t2 = t1.replace('"',"")
                    t3 = t2.replace("{","")
                    t4 = t3.replace("[","")
                    t5 = t4.replace("}","")
                    t6 = t5.replace("]","")
                    t7 = t6.replace("key,","")
                    t8 = t7.replace("value,","")
                    metadatas = t8.split(',')
                    for metadata in metadatas:
                        if (IndTitulo==1):
                            ItemTitulo = metadata
                            IndTitulo = 2
                        if IndAutor == 1:
                            ItemAutor = metadata
                            IndAutor = 2
                        if IndDesc == 1:
                            ItemDesc = metadata
                            IndDesc = 2
                        if IndFecha == 1:
                            fecha = metadata.split('T')
                            ItemFecha = fecha[0]
                            IndFecha = 2
                        if IndPais == 1:
                            ItemPais = metadata
                            IndPais = 2
                        if IndKey == 1:
                            ItemKey = metadata
                            IndKey = 2

                        if metadata=="lom.general.title":
                            IndTitulo = 1
                        if metadata == "lom.lifecycle.contribute-role":
                            IndAutor = 1
                        if metadata == "lom.general.description":
                            IndDesc = 1
                        if metadata == "dc.date.available":
                            IndFecha = 1
                        if metadata == "lom.technical.location":
                            IndPais = 1
                        if metadata == "lom.general.keyword":
                            IndKey = 1

                        if (IndTitulo==2 and IndAutor==2 and IndDesc==2 and IndFecha==2 and IndPais==2 and IndKey==2):
                            if pKey == "":
                                Items.append(DItem(idItem = ItemId,titulo = ItemTitulo, autor = ItemAutor, descripcion = ItemDesc, pais = ItemPais, fecha = ItemFecha, keywords = ItemKey))
                            else:
                                if ItemKey == pKey:
                                    Items.append(DItem(idItem = ItemId,titulo = ItemTitulo, autor = ItemAutor, descripcion = ItemDesc, pais = ItemPais, fecha = ItemFecha, keywords = ItemKey))
                            IndTitulo = 0
                            IndAutor = 0
                            IndDesc = 0
                            IndFecha = 0
                            IndPais = 0
                            IndKey = 0
                            ItemTitulo = ""
                            ItemAutor = ""
                            ItemDesc = ""
                            ItemFecha = ""
                            ItemPais = ""
                            ItemKey = ""

                    IndId1 = 0
                    ItemId = 0
            IndId2 = 0
            CollectionId = 0
    return Items


def ver_recurso(request,identificador):

    #Se inicializan variables opcionales por si viene vacias
    recursoWebsite=''
    recursoEstructura=''
    recursoVersion=''
    recursoMomentoC=''
    recursoCoordenadas=''
    recursoIdentCatalogo=''
    recursoEsquema=''
    recursoCambios=''
    recursoLenguajeDatos=''
    entidadNombre=''
    tecnicaPrevisualizacion=''
    tecnicaInstInstalacion=''
    tecnicaOtrosReq=''
    educativaInteractividad=''
    educativaDensidadSem=''
    educativaDes=''
    educativaRangoEdad=''
    educativaTiempoA=''
    educativaDificultad=''
    comentariosComent=''
    cometariosDesc=''
    comentariosFecha=''
    cometariosRating=''
    derechosRestricciones=''
    derechosCosto=''
    derechosDesc=''

    r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items/"+identificador+"/metadata")

    data = r.text
    data = data.replace('[',"")
    data = data.replace(']',"")
    dataList = data.split("},{")
    for dato in dataList:
        dato=dato.replace('{',"")
        dato=dato.replace('}',"")
        dato=dato.replace('"',"")
        dato=dato.replace(',language:null',"")
        dato=dato.replace('key:',"")
        dato=dato.replace('value:',"")
        index= dato.find(",")
        if dato[:index] == 'lom.rights.description':
            derechosDesc =  dato[index+1:]
        if dato[:index] == 'lom.rights.cost':
            derechosCosto =  dato[index+1:]
        if dato[:index] == 'lom.annotation.date':
            comentariosFecha =  dato[index+1:]
        if dato[:index] == 'lom.metadata.scheme':
            recursoEsquema =  dato[index+1:]
        if dato[:index] == 'lom.lifecycle.version':
            recursoVersion =  dato[index+1:]
        if dato[:index] == 'lom.technical.installationinstructions-otherrequirements':
            tecnicaOtrosReq =  dato[index+1:]
        if dato[:index] == 'lom.general.coverage-netsites':
            recursoWebsite =  dato[index+1:]
        if dato[:index] == 'lom.general.title':
            titulo =  dato[index+1:]
        if dato[:index] == 'lom.educational.dificult':
            educativaDificultad =  dato[index+1:]
        if dato[:index] == 'lom.general.keyword':
            palabrasClaves =  dato[index+1:]
        if dato[:index] == 'lom.lifecycle.contribute-role':
            autor =  dato[index+1:]
        if dato[:index] == 'lom.educational.description':
            educativaDes =  dato[index+1:]
        if dato[:index] == 'lom.rights.copyrightandotherrestrictions':
            derechosRestricciones =  dato[index+1:]
        if dato[:index] == 'lom.educational.level':
            nivelEducativo =  dato[index+1:]
        if dato[:index] == 'lom.educational.agerange':
            educativaRangoEdad =  dato[index+1:]
        if dato[:index] == 'lom.technical.location':
            pais =  dato[index+1:]
        if dato[:index] == 'lom.educational.typeofinteractivity':
            educativaInteractividad =  dato[index+1:]
        if dato[:index] == 'lom.annotation.entity':
            comentariosComent =  dato[index+1:]
        if dato[:index] == 'lom.annotation.description':
            cometariosDesc =  dato[index+1:]
        if dato[:index] == 'lom.metadata.identifier-catalog':
            recursoIdentCatalogo =  dato[index+1:]
        if dato[:index] == 'lom.general.description':
            descripcion =  dato[index+1:]
        if dato[:index] == 'lom.technical.previews':
            tecnicaPrevisualizacion =  dato[index+1:]
        if dato[:index] == 'lom.general.coverage-contextualmoment':
            recursoMomentoC =  dato[index+1:]
        if dato[:index] == 'lom.general.structure':
            recursoEstructura =  dato[index+1:]
        if dato[:index] == 'lom.lifecycle.changelog':
            recursoCambios =  dato[index+1:]
        if dato[:index] == 'lom.lifecycle.contribute-entity':
            entidadNombre =  dato[index+1:]
        if dato[:index] == 'lom.educational.semanticdensity':
            educativaDensidadSem =  dato[index+1:]
        if dato[:index] == 'lom.general.language':
            lenguaje =  dato[index+1:]
        if dato[:index] == 'lom.general.aggregationlevel':
            nivelAgregacion =  dato[index+1:]
        if dato[:index] == 'lom.educational.timelearning':
            educativaTiempoA =  dato[index+1:]
        if dato[:index] == 'lom.technical.requirement-orcomponent-type':
            tipoRecurso =  dato[index+1:]
        if dato[:index] == 'lom.metadata.language':
            recursoLenguajeDatos =  dato[index+1:]
        if dato[:index] == 'lom.technical.format':
            extension =  dato[index+1:]
        if dato[:index] == 'lom.lifecycle.contribute-date':
            fecha =  dato[index+1:]
        if dato[:index] == 'lom.annotation.setrating':
            cometariosRating =  dato[index+1:]
        if dato[:index] == 'lom.general.coverage-geolocationcoordinates':
            recursoCoordenadas =  dato[index+1:]
        if dato[:index] == 'lom.technical.installationinstructions':
            tecnicaInstInstalacion =  dato[index+1:]

    #Se obtiene la categoria
    r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items/"+identificador+"?expand=parentCollection")
    data = r.text
    dataList = data.split('"parentCollection":')
    data = dataList[1]
    index= data.find("name")
    data = data[index+7:]
    index = data.find('"')
    categoria = data[:index]

    #Se obtiene el area de conocimiento
    r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items/"+identificador+"?expand=parentCommunityList")
    data = r.text
    dataList = data.split('"parentCommunityList":')
    data = dataList[1]
    index= data.find("name")
    data = data[index+7:]
    index = data.find('"')
    areaConocimiento = data[:index]

    item = MetadataItem(titulo=titulo,
                        descripcion=descripcion,
                        autor=autor,
                        nivelAgregacion=nivelAgregacion,
                        extension=extension,
                        lenguaje=lenguaje,
                        pais=pais,
                        palabrasClaves=palabrasClaves,
                        fecha=fecha,
                        areaConocimiento=areaConocimiento,
                        categoria=categoria,
                        tipoRecurso=tipoRecurso,
                        nivelEducativo=nivelEducativo,
                        recursoWebsite=recursoWebsite,
                        recursoEstructura=recursoEstructura,
                        recursoVersion=recursoVersion,
                        recursoMomentoC=recursoMomentoC,
                        recursoCoordenadas=recursoCoordenadas,
                        recursoIdentCatalogo=recursoIdentCatalogo,
                        recursoEsquema=recursoEsquema,
                        recursoCambios=recursoCambios,
                        recursoLenguajeDatos=recursoLenguajeDatos,
                        entidadNombre=entidadNombre,
                        tecnicaPrevisualizacion=tecnicaPrevisualizacion,
                        tecnicaInstInstalacion=tecnicaInstInstalacion,
                        tecnicaOtrosReq=tecnicaOtrosReq,
                        educativaInteractividad=educativaInteractividad,
                        educativaDensidadSem=educativaDensidadSem,
                        educativaDes=educativaDes,
                        educativaRangoEdad=educativaRangoEdad,
                        educativaTiempoA=educativaTiempoA,
                        educativaDificultad=educativaDificultad,
                        comentariosComent=comentariosComent,
                        cometariosDesc=cometariosDesc,
                        comentariosFecha=comentariosFecha,
                        cometariosRating=cometariosRating,
                        derechosRestricciones=derechosRestricciones,
                        derechosCosto=derechosCosto,
                        derechosDesc=derechosDesc,
    )

    return render_to_response('ver_recurso.html', {'item':item,'request':request,'identificador':identificador},context_instance=RequestContext(request))


@login_required()
def eliminarRecurso(request,identificador):
    # Se valida si se ha presionado el botón continuar
    #if request.method=='POST':



        # Se loguea en dspace
        parametros = {
           "email":os.environ.get('EMAIL_DSPACE'),
           "password":os.environ.get('PASSWORD_DSPACE')
            }
        r = requests.post("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/login", json=parametros)
        #r = requests.post("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/login", json=parametros)
        print(r.status_code, r.text)
        token = r.text.strip("(")
        token = r.text.strip(")")


        # Verifica el estado del usuario
        headers = {"rest-dspace-token": token,
                       "content-type": "application/json",
                       "Accept": "application/json"}

        r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/status", headers=headers)
        #r = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/status", headers=headers)
        print(r.status_code, r.text)

        r = requests.delete("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items/"+identificador, headers=headers)
        #r = requests.delete("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/items/"+identificador, headers=headers)
        print(r.status_code, r.text)


        r = requests.delete("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items/"+identificador+"/metadata", headers=headers)
        #r = requests.delete("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/items/"+identificador+"/metadata",headers=headers)
        print(r.status_code, r.text)


        r = requests.delete("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/bitstreams/"+identificador, headers=headers)
        #r = requests.delete("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/bitstreams/"+identificador,headers=headers)
        print(r.status_code, r.text)


        return HttpResponseRedirect("/")




def recursos(request,identificador):

        r = requests.get("http://"+os.environ.get('DNS_DSPACE')+":8080/rest/items/"+identificador+"/bitstreams")
        #r = requests.get("http://ec2-54-86-20-91.compute-1.amazonaws.com:8080/rest/items/"+identificador+"/bitstreams")


        r1 = r.text.replace(":",",")
        r2 = r1.replace('"',"")
        r3 = r2.replace("{","")
        r4 = r3.replace("[","")
        datos = r4.split(',')
        valor=0
        links=""
        formatos=""
        for dato in datos:

            if (valor==1):
                formatos=dato
                valor=2

            if (valor==3):
                links=dato
                valor=4

            if (dato=="format" and valor==0):
                valor=1

            if (dato=="retrieveLink" and valor==2):
                valor=3


        return HttpResponseRedirect("http://"+os.environ.get('DNS_DSPACE')+":8080/rest"+links)
